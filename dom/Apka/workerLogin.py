import sys
sys.path.insert(0, 'libs')

from libs import redis
from libs.rq.decorators import job
from google.appengine.ext import db

conn = redis.StrictRedis(host='pub-redis-15244.us-east-1-2.1.ec2.garantiadata.com', port=15244)

@job('default', connection=conn)
def logowanie(login, haslo):

    userTemp = db.GqlQuery("SELECT * FROM User where login='"+login+"'")

    if userTemp.count() > 0:
        if userTemp[0].haslo == haslo:
            return 'zalogowany'
        else:
            return 'blad'
    else:
        return 'blad'