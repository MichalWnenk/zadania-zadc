<!DOCTYPE html>
<html>
    <style type="text/css">
        #wpisy
        {
	        position: relative;
	        text-align:center;
	    }
	    #calosc
	    {
	        margin: 0 auto;
	        background-color: green;
	    }

	    *.odstep
	    {
	        padding: 10px;
	    }
	    *.na_srodek
	    {
	        position: relative;
	        text-align:center;
	    }
    </style>
    <head>
        <title>mBlog : rejestracja</title>
    </head>
    <body id="calosc">
    <div class="na_srodek">
        <h1 class="odstep">Rejestracja nie powiodła się :(</h1>
        <div> Nie wypełniłeś wszystkich pól lub login jest już zajęty :( </div>
        <div class="odstep"><a href='/register'>Spróbuj zarejestrować się ponownie</a></div>
        </div>
    </body>
</html>