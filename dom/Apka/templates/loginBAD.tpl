<!DOCTYPE html>
<html>
    <style type="text/css">
        #wpisy
        {
	        position: relative;
	        text-align:center;
	    }
	    #calosc
	    {
	        margin: 0 auto;
	        background-color: green;
	    }

	    *.odstep
	    {
	        padding: 10px;
	    }
	    *.na_srodek
	    {
	        position: relative;
	        text-align:center;
	    }
    </style>
    <head>
        <title>mBlog : logowanie</title>
    </head>
    <body id="calosc">
    <div class="na_srodek">
        <h1 class="odstep">Logowanie nie powiodło się :(</h1>
        <div> Podałeś nieprawidłowy login lub hasło :( </div>
        <div class="odstep"><a href='/login'>Spróbuj zalogować się ponownie</a></div>
        </div>
    </body>
</html>