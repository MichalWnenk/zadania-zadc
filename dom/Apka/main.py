# -*- coding: utf-8 -*-
__author__ = 'MadMike'

from framework.bottle import Bottle
from framework.bottle import route, template, request, error, debug, response
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db
from models import User
from workerLogin import logowanie

app = Bottle()

session = ['','']

@app.route('/')
def index():
    if  session[0] == '':
        return template('templates/index')
    else:
        return template('templates/loginOK', login=session[0])

@app.error(404)
def error404(error):
    return template('wrongPath')
	
@app.route('/login')
@app.route('/login', method='POST')
def login():

    if session[0] == '':
        if request.method == 'GET':
            return template('templates/login')


        if request.method == 'POST' and session[0] == '':
            login = request.forms.login
            haslo = request.forms.haslo

            info = logowanie(login=login, haslo=haslo)

            if info == 'zalogowany':
                session[0] = login
                session[1] = haslo
                return template('templates/loginOK', login=login)
            elif info == 'blad':
                return template('templates/loginBAD')
    else:
        return template('templates/loginOK', login=session[0])


@app.route('/register')
@app.route('/register', method='POST')
def register():
    if request.method == 'GET':
        return template('templates/register')

    if request.method == 'POST':
        imie = request.forms.imie
        nazwisko = request.forms.nazwisko
        login = request.forms.login
        haslo = request.forms.haslo

        userTemp = db.GqlQuery("SELECT * FROM User where login='"+login+"'")

        if imie and nazwisko and login and haslo != '':
            if userTemp.count() == 0:
                u = User(imie=imie, nazwisko=nazwisko, login=login, haslo=haslo)
                u.put()
                return template('templates/registerOK')
            else:
                return template('templates/registerBAD')
        else:
            return template('templates/registerBAD')

@app.route('/users')
def users():
    usersList = db.GqlQuery("SELECT * FROM User")

    return template('templates/users_list',ludzie=usersList)