from google.appengine.ext import db
from google.appengine.api import users

class User(db.Model):
    imie = db.StringProperty(required=True)
    nazwisko = db.StringProperty(required=True)
    login = db.StringProperty(required=True)
    haslo = db.StringProperty(required=True)
