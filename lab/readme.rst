Laboratorium #C
===============

1. Uruchomić na Heroku dowolną swoją aplikację webową z wcześniejszych zajęć.

2. Dodatkowo wzbogacić powyższą aplikację o cache'owanie z wykorzystaniem Redisa lub Memcached
   w chmurze (np. `<https://redislabs.com>`__) i zadanie wykonywane w tle. Można w tym celu
   wykorzystać pracę domową sprzed poprzednich zajęć.
